/***********
Nathan Biyani
nkbiyan
Lab 1021
Section 4
Alex Myers
************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));



  const int TOTAL_NUM_CARDS = 52;
  Card arrCards[TOTAL_NUM_CARDS];
  for (int i = 0; i < TOTAL_NUM_CARDS; i++)
  {
	Suit value = static_cast<Suit>((i/13));
  	arrCards[i].suit = value;
	arrCards[i].value = (i%13) + 2;

	
  }
	
  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/


  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	random_shuffle(arrCards, arrCards + 52, myrandom);
	
	 


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

	Card arrHand[5];
	for (int i = 0; i < 5; i=i+1)
	{
		arrHand[i] = arrCards[i];
		cout << get_card_name(arrHand[i]) << " of " << get_suit_code(arrHand[i]) << endl;
	}




	sort(arrHand, arrHand + 5, suit_order);
	for (int i = 0; i < 5; i=i+1)
	{
		cout << setw(10) << right << get_card_name(arrHand[i]) << " of " << get_suit_code(arrHand[i]) << endl;
	}


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  bool suit = false;
  if (lhs.suit < rhs.suit)
  {
	suit = true;
  }
  else if (lhs.suit > rhs.suit)
  {
	suit = false;
  }
  else if (rhs.suit == lhs.suit)
  {
     if (lhs.value < rhs.value)
	{
	suit = true;
	}
     if (rhs.value < lhs.value)
     {
	suit = false;
     }
  }

  return suit;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  string value;
	if (c.value == 2)
	{
		value = "2";
	}
	else if (c.value == 3)
	{
		value = "3";
	}
	else if (c.value == 4)
	{
		value = "4";
	}
	else if (c.value == 5)
	{
		value =  "5";
	}
	else if (c.value == 6)
	{
		value =  "6";
	}
	else if (c.value == 7)
	{
		value =  "7";
	}
	else if (c.value == 8)
	{
		value =  "8";
	}
	else if (c.value == 9)
	{
		value =  "9";
	}
	else if (c.value == 10)
	{
		value =  "10";
	}
	else if (c.value == 11)
	{
		value =  "Jack";
	}
	else if (c.value == 12)
	{
		value =  "Queen";
	}
	else if (c.value == 13)
	{
		value =  "King";
	}
	else if (c.value == 14)
	{
		value =  "Ace";
	}

	return value;

}



